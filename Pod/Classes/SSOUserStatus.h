//
//  SSOUserStatus.h
//  Pods
//
//  Created by Pankaj Verma on 05/04/17.
//
//

#import <Foundation/Foundation.h>

@interface SSOUserStatus : NSObject
//User Status
@property (nonatomic, nullable, readonly) NSString * identifier;
@property (nonatomic, nullable, readonly) NSString * status;
@property (nonatomic, nullable, readonly) NSString * statusCode;

- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;
@end
