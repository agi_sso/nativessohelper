//
//  SSOError.m
//  Pods
//
//  Created by Pankaj Verma on 06/04/17.
//
//

#import "SSOError.h"

@implementation SSOError
+ (SSOError *)errorWithCode:(SSOSDKErrorCode)code
{
    NSString *errorDescription = @"";
    switch (code)
    {
        case SSOSDKErrorCodeUnauthorizedAccess:
            errorDescription = @"session expired";
            break;
        case    SSOSDKErrorCodeInvalidRequest:// 413,
            errorDescription = @"INVALID_REQUEST";
            break;
            
        case    SSOSDKErrorCodeSDKNotInitialized:
            errorDescription = @"SDK is not initialized";
            break;
        case SSOSDKErrorCodeMigrateUnverifiedUserError :
            errorDescription = @"Unverified user";
            break;
        case   SSOSDKErrorCodeDataNil:
            errorDescription = @"No data found";
            break;
        case   SSOSDKErrorCodeNotString:
            errorDescription = @"Field is not string";
            break;
        case   SSOSDKErrorCodeNotBoolean:
            errorDescription = @"field is not boolean";
            break;
        case   SSOSDKErrorCodeSessionNotFound:
            errorDescription = @"no session found";
            break;
        case  SSOSDKErrorCodeOauthIdNotFound:
            errorDescription = @"oauth id not found";
            break;
        case  SSOSDKErrorCodeOauthSiteIdNotFound:
            errorDescription = @"oauth site id not found";
            break;
        case  SSOSDKErrorCodeAccessTokenNotFound:
            errorDescription = @"access token not found";
            break;
        case  SSOSDKErrorCodeFbLoginFlowCancelled:
            errorDescription = @"user cancelled fb login flow";
            break;
        case  SSOSDKErrorCodePayloadNotFound:
            errorDescription = @"payload is missing";
            break;
        case  SSOSDKErrorCodeSignatureNotFound:
            errorDescription = @"signature is missing";
            break;
        case  SSOSDKErrorCodeResponseCodeNotFound:
            errorDescription = @"no response from server";
            break;
        case   SSOSDKErrorCodeFailureInfoNotFound:
            errorDescription = @"no failure description from server";
            break;
        case  SSOSDKErrorCodeAppIdentifierPrefixMissing:
            errorDescription = @"AppIdentifierPrefix  is missing in project's plist";
            break;
        case  SSOSDKErrorCodeChannelMissing:
            errorDescription = @"channel is missing in project's plist";
            break;
        case  SSOSDKErrorCodeSiteIdMissing:
            errorDescription = @"siteId  is missing in project's plist";
            break;
        case  SSOSDKErrorCodePicSizeExceeded:
            errorDescription = @"Maximum acceptable image size is up to 2MB";
            break;
        case  SSOSDKErrorCodeTermsInvalidString:
            errorDescription = @"termsAccepted property should have string value eithe 0 or 1";
            break;
        case  SSOSDKErrorCodeShareDataInvalidString:
            errorDescription = @"shareDataAllowed property should have string value eithe 0 or 1";
            break;
    
        default:
            break;
    }
    return [self errorWithCode:code description:errorDescription];
}

+ (SSOError *)errorWithCode:(SSOSDKErrorCode)code
                description:(NSString *)errorDescription
{
    return [[self class] errorWithDomain:SSOSDKErrorDomain
                                    code:code
                                userInfo:@{SSOSDKErrorDescription : errorDescription ? errorDescription : @""}];
}

- (SSOSDKErrorCode)getErrorCode
{
    return (SSOSDKErrorCode)self.code;
}
@end
