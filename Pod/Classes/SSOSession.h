//
//  SSOSession.h
//  Pods
//
//  Created by Pankaj Verma on 04/04/17.
//
//

#import <Foundation/Foundation.h>
#import "SSOBasicUserDetails.h"
#import "SSOUserDetails.h"


typedef NS_OPTIONS(NSUInteger, SSOSessionOption)
{
    SSOGlobalSession = (1UL << 0),
    SSOAppSession = (1UL << 1)
} NS_ENUM_AVAILABLE(10_7, 5_0);


@interface SSOSession : NSObject
@property (nonatomic, nullable, readonly) NSString *ssec;
@property (nonatomic, nullable, readonly) NSString *ticketId;
@property (nonatomic, nullable, readonly) NSString *tgId;
@property (nonatomic, nullable, readonly) NSString *type;
@property (nonatomic, nullable, readonly) NSString *identifier;
//@property (nonatomic, nullable, readonly) NSString *unverifiedUser;

@property (nonatomic, nullable) SSOBasicUserDetails *user;
@property (nonatomic, nullable) SSOUserDetails *userDetails;

- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;
- (nullable instancetype)initWithAppKeychain;
- (nullable instancetype)initWithSharedKeychain;

-(void)setTgId:(NSString * _Nullable)tgId;
-(void)setTicketId:(NSString * _Nullable)ticketId;
-(void)setSsec:(NSString * _Nullable)ssec;
-(void)setType:(NSString * _Nullable)type;
-(void)setIdentifier:(NSString * _Nullable)identifier;
//-(void)setUnverifiedUser:(NSString * _Nullable)unverifiedUser;

-(NSString *_Nullable)ssec;
-(NSString *_Nullable)ticketId;
-(NSString *_Nullable)tgId;
-(NSString *_Nullable)type;
-(NSString *_Nullable)identifier;
-(NSString *_Nullable)unverifiedUser;

-(void)removeAppSessionSsec;
-(void)removeAppSessionTicketId;
-(void)removeAppSessionIdentifier;
-(void)removeAppSessionType;
-(void)removeAppSessionTgId;

-(void)removeSharedSessionSsec;
-(void)removeSharedSessionTicketId;
-(void)removeSharedSessionIdentifier;
-(void)removeSharedSessionType;
-(void)removeSharedSessionTgId;

-(void)initializeAppSessionWithSession:(SSOSession *_Nullable)session;
-(void)initializeSharedSessionWithSession:(SSOSession *_Nullable)session;
-(void)initializeAppSessionWithTgId:(NSString *_Nullable)tgId;
-(void)initializeGlobalSessionWithTgId:(NSString *_Nullable)tgId;
-(void)setAppSessionNewTicketId:(NSString *_Nullable)ticket;
-(void)updateAppSession;
-(void)updateGlobalSession;

-(void)saveUserDetailsToKeychain:(SSOUserDetails *_Nonnull)userDetails;
-(void)RemoveUserDetailsFromKeychain;
-(SSOUserDetails *_Nullable) getUserDetailsFromKeychain;
@end
