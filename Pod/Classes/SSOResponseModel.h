//
//  SSOResponseModel.h
//  Pods
//
//  Created by Pankaj Verma on 03/05/17.
//
//

#import <Foundation/Foundation.h>

@interface SSOResponseModel : NSObject

@property (nonatomic, nullable) NSString *code;
@property (nonatomic, nullable) NSString *message;
@property (nonatomic, nullable) NSString *msg;
@property (nonatomic, nullable) NSDictionary *data;
@property (nonatomic, nullable) NSDictionary *status;

- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;
@end
