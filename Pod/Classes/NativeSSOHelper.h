//
//  SSOEnums.h
//  Pods
//
//  Created by Amit Kumar12 on 17/05/18.
//

#import "SSOSession.h"
#import "SSOBasicUserDetails.h"
#import "SSOUserStatus.h"
#import "SSOUserDetails.h"
#import "SSOUserUpdates.h"
#import "SSOError.h"
#import "SSOSocialInfo.h"
#import "SSOSignupUser.h"

typedef NS_OPTIONS(NSUInteger, SSOSocialActivityOptions)
{
    SSOFacebookLogin = (1UL << 0),
    SSOGoogleLogin = (1UL << 1),
    SSOTruecallerLogin = (1UL << 2),
    SSOFacebookLink = (1UL << 3),
    SSOGoogleLink = (1UL << 4),
    SSOFacebookPicUpload = (1UL << 5),
    SSOGooglePicUpload = (1UL << 6),
    SSOFacebookDelink = (1UL << 7),
    SSOGoogleDelink = (1UL << 8),
    SSOGsmaLogin =  (1UL << 9)
} NS_ENUM_AVAILABLE(10_7, 5_0);

typedef NS_OPTIONS(NSUInteger, SSOSignupOptions)
{
    SSOFullSignup = (1UL << 0),
    SSOOnlyMobileSignup = (1UL << 1)
} NS_ENUM_AVAILABLE(10_7, 5_0);

typedef NS_OPTIONS(NSUInteger, SSOPickUploadOptions)
{
    SSOCamera = (1UL << 0),
    SSOPhotoGallery = (1UL << 1),
    SSODefault = (1UL << 2)
} NS_ENUM_AVAILABLE(10_7, 5_0);


typedef void (^errorBlock) (SSOError *  _Nullable error);
typedef void (^voidBlock) (void);
typedef void (^successBlock) (NSDictionary * _Nullable info);
typedef void (^completionBlock) (NSDictionary * _Nullable info,SSOError * _Nullable error);
typedef void (^sessionBlock) (SSOSession * _Nullable  session, SSOError * _Nullable error);
typedef void (^userStatusBlock) (SSOUserStatus * _Nullable user);
typedef void (^userDetailsBlock) (SSOUserDetails * _Nullable user,SSOError * _Nullable error);
typedef void (^userUpdatesBlock) (SSOUserUpdates * _Nullable user);
typedef void (^imageUploadStart) (void);
