//
//  NSSOGlobal.m
//  Pods
//
//  Created by Pankaj Verma on 10/13/16.
//
//

#import "NSSOGlobal.h"
@implementation NSSOGlobal
//Relative Urls
 NSString * const getDataForDeviceUrlPath = @"/sso/app/getDataForDevice";//Old
 NSString * const getSsecFromTicket = @"getSsecFromTicket";
 NSString * MIGRATE_TICKETID = @"";
 NSString * UUID_BOUNDARY = @"";

 SSOSession * appSession = nil;
 SSOSession * globalSession = nil;
 SSOHeaders * appHeader = nil;
@end
