//
//  SSOSocialInfo.h
//  Pods
//
//  Created by Pankaj Verma on 20/04/17.
//
//

#import <Foundation/Foundation.h>
#define SSOSOCIAL_OAUTH_ID_KEY @"oauthId"
#define SSOSOCIAL_ACCESS_TOKEN_KEY @"accessToken"
#define SSOSOCIAL_PAYLOAD_KEY @"payload"
#define SSOSOCIAL_SIGNATURE_KEY @"signature"
#define USER_MOBILE_PHONE @"user_mobile_phone"
#define SSOSOCIAL_GSMA_TOKEN_KEY @"token"
#define SSOSOCIAL_GSMA_PHONE_NUMBER_KEY @"phoneNumber"

#define SSOSOCIAL_OAUTH_SITEID_KEY @"oauthsiteid"
#define SSOSOCIAL_OAUTH_SITEID_NEW_KEY @"oauthSiteId"

//login Types
#define SSOSOCIAL_TYPE_GOOGLEPLUS  @"googleplus"
#define SSOSOCIAL_TYPE_FACEBOOK  @"facebook"
#define SSOSOCIAL_TYPE_SSO  @"sso"
#define SSOSOCIAL_TYPE_TRUECALLER  @"truecaller"
#define SSOSOCIAL_TYPE_GSMA  @"gsma"



@interface SSOSocialInfo : NSObject

//Facebook and Google
@property (nonatomic, nullable) NSString *oauthId;
@property (nonatomic, nullable) NSString *accessToken;
// if Facebook asks for mobile
@property Boolean user_mobile_phone;
//Truecaller
@property (nonatomic, nullable) NSString *payload;
@property (nonatomic, nullable) NSString *signature;
//Gsma
@property (nonatomic, nullable) NSString *gsmaToken;
@property (nonatomic, nullable) NSString *phoneNumber;
- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;

@end
