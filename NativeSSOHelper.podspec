Pod::Spec.new do |s|
  s.name             = "NativeSSOHelper"
  s.version          = "1.1.0"
  s.summary          = "NativeSSOHelper Contains Models for Native SSO SDK"

  s.description      = <<-DESC
                       NativeSSOHelper is used cross models pod for tilsdk.
                       DESC

  s.homepage         = "http://tilsdk.indiatimes.com/"
  s.license          =  { :type => 'PRIVATE' }
  s.author           = { "Pankaj Verma" => "pankaj.verma@timesinternet.in" }
  s.source           = { :git=> "https://bitbucket.org/agi_sso/nativessohelper.git" }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
end
